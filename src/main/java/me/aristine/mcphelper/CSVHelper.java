package me.aristine.mcphelper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import lombok.Cleanup;
import lombok.NonNull;

public class CSVHelper {
    
    public static ArrayList<String[]> parse(@NonNull ResourceFile csvFile, int rowWidth) {
        return parse(csvFile, rowWidth, ",");
    }
    
    public static ArrayList<String[]> parse(@NonNull ResourceFile csvFile, int rowWidth, String splitChar) {
        return parse(csvFile, rowWidth, splitChar, false);
    }
    
    public static ArrayList<String[]> parse(@NonNull ResourceFile csvFile, int rowWidth, String splitChar, boolean includeTitle) {
        boolean hasSkipped = includeTitle;
        ArrayList<String[]> result = new ArrayList<String[]>();
        try {
           @Cleanup InputStreamReader iReader = new InputStreamReader(csvFile.getInputStream());
           
           @Cleanup BufferedReader lineReader = new BufferedReader(iReader);
           String currentLine;
           while((currentLine = lineReader.readLine()) != null) {
               if (!hasSkipped) {
                   hasSkipped = true;
                   continue;
               }                   
               result.add(currentLine.split(splitChar, -1));
               
           }
            
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        return result;
    }
}
