package me.aristine.mcphelper.examplemod;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.HashSet;

import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;
import javassist.NotFoundException;
import me.aristine.mcphelper.MCPMappingHelper;
import me.aristine.mcphelper.MCPMappingHelper.MCPMapping;
import net.minecraft.launchwrapper.IClassTransformer;

/**
 *  Example Transformer Class, which uses Javassist to rewrite two methods of net.minecraft.world.biome.Biome
 *  By using MCPMappingHelper, the proper function name(either searge or deobf representation) is returned.
 *  Note however, this example doesn't verify method descriptors, which should be done to avoid trying to 
 *       overwrite getTemperature(BlockPos pos) when you want to overwrite getTemperature()
 * @author Aristine
 */
public class ExampleTransformer implements IClassTransformer {
    private String isSnowyBiome, getTemperature;

    @Override
    public byte[] transform(String name, String transformedName, byte[] basicClass) {
        if (transformedName.equals(ExampleCoreMod.POINTERS.CLASS.BIOME)) {
            try {
                ClassPool pool = ClassPool.getDefault();
                CtClass modified = pool.makeClass(new ByteArrayInputStream(basicClass));
                MCPMapping mapping = MCPMappingHelper.INSTANCE().getMapping();
                /*
                 * Since deobf method names aren't unique, we need to provide a set of possible method 
                 * representations(all of the currently declared methods of the class) alongside our methodName
                 */
                isSnowyBiome = mapping.getMethodName(ExampleCoreMod.POINTERS.METHOD.IS_SNOWY_BIOME, toMethodSet(modified.getMethods()));
                getTemperature = mapping.getMethodName(ExampleCoreMod.POINTERS.METHOD.GET_TEMPERATURE, toMethodSet(modified.getMethods()));
                transformMethod(modified, isSnowyBiome, isSnowyBiome());
                transformMethod(modified, getTemperature, getTemperature());
                return modified.toBytecode();
            } catch (CannotCompileException | IOException | NotFoundException e) {
                try {
                    CtClass modified = ClassPool.getDefault().makeClass(new ByteArrayInputStream(basicClass));
                    System.out.println("FAILED: " + isSnowyBiome + " : " + getTemperature);
                    for (CtMethod m : modified.getDeclaredMethods()) {
                        System.out.println(m.getName() + " : "  + modified.getDeclaredMethod(isSnowyBiome) + " : " + modified.getDeclaredMethods(getTemperature));
                    }
                } catch (Exception f) {
                    f.printStackTrace();
                }
                e.printStackTrace();
            }
        }
        return basicClass;
    }

    private HashSet<String> toMethodSet(CtMethod... methods) {
        HashSet<String> result = new HashSet<String>();
        for (CtMethod m : methods) {
            result.add(m.getName());
        }
        return result;
    }

    private CtClass transformMethod(CtClass orig, String methodName, String newMethodBody) throws CannotCompileException, NotFoundException {
        orig.getDeclaredMethod(methodName).setBody(newMethodBody);
        return orig;
    }

    private final String isSnowyBiome() {
        StringBuilder result = new StringBuilder();
        result.append("{");
        result.append(String.format("return this.%s() < 0.15f;", getTemperature));
        result.append("}");
        return result.toString();
    }

    private final String getTemperature() {
        StringBuilder result = new StringBuilder();
        result.append("{");
        result.append("return 0.0f;");
        result.append("}");
        return result.toString();
    }
}
