package me.aristine.mcphelper.examplemod;

import java.util.Map;

import net.minecraft.launchwrapper.Launch;
import net.minecraftforge.fml.relauncher.IFMLLoadingPlugin;
import net.minecraftforge.fml.relauncher.IFMLLoadingPlugin.DependsOn;
import net.minecraftforge.fml.relauncher.IFMLLoadingPlugin.SortingIndex;

/**
 * Entry point of the example core mod
 * @author Aristine
 */

@SortingIndex(1001) //IMPORTANT that his is >= 1001 : otherwise, core mod will load before searge conversions and possible before MCPHelper.
public class ExampleCoreMod implements IFMLLoadingPlugin {
    

    public static final class POINTERS {

        public static final class CLASS {
            public static final String BIOME = "net.minecraft.world.biome.Biome";
        }

        public static final class METHOD {
            public static final String IS_SNOWY_BIOME = "isSnowyBiome", GET_TEMPERATURE = "getTemperature";
        }

    }

    @Override
    public String[] getASMTransformerClass() {
        return new String[] { ExampleTransformer.class.getCanonicalName() };
    }

    @Override
    public String getModContainerClass() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getSetupClass() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void injectData(Map<String, Object> data) {
        if (!Launch.blackboard.containsKey("mcphelper.present")) {
            //Error out now, mcphelper was not loaded!
            throw new RuntimeException("ExampleMod Transformer failed due to MCPHelper not being present");
        }
    }

    @Override
    public String getAccessTransformerClass() {
        // TODO Auto-generated method stub
        return null;
    }

}
