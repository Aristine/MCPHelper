package me.aristine.mcphelper;

import java.util.Map;

import net.minecraft.launchwrapper.Launch;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.relauncher.IFMLLoadingPlugin;

public class MCPHelperCore implements IFMLLoadingPlugin {
    
    @Override
    public String[] getASMTransformerClass() {
        return new String[] { };
    }

    @Override
    public String getModContainerClass() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getSetupClass() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void injectData(Map<String, Object> data) {
        String version = ((Map<String, String>) Launch.blackboard.get("launchArgs")).get("--assetIndex");
        MCPMappingHelper.INSTANCE().setDefaultVersion(version);
        MCPMappingHelper.INSTANCE().registerMapping();
        Launch.blackboard.put("mcphelper.present", true);
    }

    @Override
    public String getAccessTransformerClass() {
        // TODO Auto-generated method stub
        return null;
    }
}
