package me.aristine.mcphelper;

import java.net.URI;
import java.util.HashMap;
import java.util.HashSet;

import lombok.NonNull;
import net.minecraft.launchwrapper.Launch;

public class MCPMappingHelper {
    private static MCPMappingHelper INSTANCE;
    private HashMap<String, MCPMapping> mappings;
    private static final String SEPERATOR = "/", MAPPINGS_PATH = "mcphelper/mappings", MAPPINGS_METHODS_NAME = "methods.csv",
            MAPPINGS_FIELDS_NAME = "fields.csv", MAPPINGS_PARAMS_NAME = "params.csv";
    private static final int MAPPINGS_OBF = 0, MAPPINGS_DEOBF = 1;
    private static final int MAPPINGS_FIELDS_WIDTH = 4, MAPPINGS_METHODS_WIDTH = 4, MAPPINGS_PARAMS_WIDTH = 3;
    private String version;

    private MCPMappingHelper() {
        mappings = new HashMap<String, MCPMapping>();
    }

    public static MCPMappingHelper INSTANCE() {
        return INSTANCE != null ? INSTANCE : (INSTANCE = new MCPMappingHelper());
    }
    
    public void setDefaultVersion(String version) {
        this.version = version;
    }

    public boolean isObfuscated() {
        return !(boolean) Launch.blackboard.get("fml.deobfuscatedEnvironment");
    }
    
    private String getDefaultMappingForVersion() {
        if (version.contains("1.12")) {
            return "snapshot_20171013";
        } else if (version.contains("1.11")) {
            return "stable_32";
        } else if (version.contains("1.10")) {
            return "stable_29";
        } else if (version.contains("1.9.4")) {
            return "stable_26";
        } else if (version.contains("1.9")) {
            return "stable_24";
        } else if (version.contains("1.7")) {
            return "stable_12";
        }
        return null;
    }
    
    public MCPMapping getMapping() {
        return getMapping(getDefaultMappingForVersion());
    }

    public MCPMapping getMapping(String mapping) {
        return mappings.get(mapping);
    }
    
    public void registerMapping() {
        registerMapping(getDefaultMappingForVersion());
    }

    public void registerMapping(String mappingName) {
        registerMapping(mappingName, SEPERATOR + MAPPINGS_PATH + SEPERATOR);
    }

    public void registerMapping(String mappingName, String mappingsRoot) {
        registerMapping(mappingName, new ResourceFile(URI.create(mappingsRoot)));
    }

    public void registerMapping(String mappingName, ResourceFile mappingsRoot) throws NullPointerException {
        if (mappingsRoot.getChild(mappingName) == null) {
            throw new NullPointerException("Bad mapping - Resource not found");
        }

        MCPMapping mapping = mappings.get(mappingName);

        if (mapping == null) {
            mapping = new MCPMapping();
        }
        
        ResourceFile csvFields = mappingsRoot.getChild(mappingName).getChild(MAPPINGS_FIELDS_NAME);
        ResourceFile csvMethods = mappingsRoot.getChild(mappingName).getChild(MAPPINGS_METHODS_NAME);
        ResourceFile csvParams = mappingsRoot.getChild(mappingName).getChild(MAPPINGS_PARAMS_NAME);

        if (csvFields != null) {
            for (String[] row : CSVHelper.parse(csvFields, MAPPINGS_FIELDS_WIDTH)) {
                mapping.registerField(row[MAPPINGS_OBF], row[MAPPINGS_DEOBF]);
            }
        }
        if (csvMethods != null) {
            for (String[] row : CSVHelper.parse(csvMethods, MAPPINGS_METHODS_WIDTH)) {
                mapping.registerMethod(row[MAPPINGS_OBF], row[MAPPINGS_DEOBF]);
            }
        }
        if (csvParams != null) {
            for (String[] row : CSVHelper.parse(csvParams, MAPPINGS_PARAMS_WIDTH)) {
                mapping.registerParam(row[MAPPINGS_OBF], row[MAPPINGS_DEOBF]);
            }
        }

        mappings.put(mappingName, mapping);
        System.out.println("MC VERSION: + " + version + " - Mapped " + mappingName + "!");
    }
    
    
    
    
    
    
    
    
    

    public class MCPMapping {
        private HashMap<String, String> methodsMapped;
        private HashMap<String, HashSet<String>> methodsLookup;
        private HashMap<String, String> fieldsMapped;
        private HashMap<String, HashSet<String>> fieldsLookup;
        private HashMap<String, String> paramsMapped;
        private HashMap<String, HashSet<String>> paramsLookup;

        public MCPMapping() {
            methodsLookup = new HashMap<String, HashSet<String>>();
            methodsMapped = new HashMap<String, String>();
            fieldsLookup = new HashMap<String, HashSet<String>>();
            fieldsMapped = new HashMap<String, String>();
            paramsLookup = new HashMap<String, HashSet<String>>();
            paramsMapped = new HashMap<String, String>();
        }
        
        private void registerBinding(HashMap<String, String> map, HashMap<String, HashSet<String>> lookupMap, String obfBinding, String deobfBinding) {
            if (!map.containsKey(obfBinding)) {
                map.put(obfBinding, deobfBinding);
                HashSet<String> lookup = lookupMap.get(deobfBinding);
                if (lookup == null) {
                    lookup = new HashSet<String>();
                }
                lookup.add(obfBinding);
                lookupMap.put(deobfBinding, lookup);
            }
        }

        public String find(@NonNull HashSet<String> definedBindings, @NonNull HashSet<String> possibleBindings) {
            if (!definedBindings.isEmpty() && !possibleBindings.isEmpty()) {
                if (definedBindings.size() < possibleBindings.size()) {
                    for (String binding : definedBindings) {
                        if (possibleBindings.contains(binding)) {
                            return binding;
                        }
                    }
                } else {
                    for (String binding : possibleBindings) {
                        if (definedBindings.contains(binding)) {
                            return binding;
                        }
                    }
                }
            }
            return null;
        }

        public String getMethodDeob(String binding) {
            return methodsMapped.get(binding);
        }

        public HashSet<String> getMethodBindings(String deobMethodName) {
            return methodsLookup.get(deobMethodName);
        }

        public String getMethodName(String deobMethodName, HashSet<String> possibleBindings) {
            return !isObfuscated() ? deobMethodName : find(getMethodBindings(deobMethodName), possibleBindings);
        }

        public String getFieldDeob(String binding) {
            return fieldsMapped.get(binding);
        }

        public HashSet<String> getFieldBindings(String fieldName) {
            return fieldsLookup.get(fieldName);
        }

        public String getFieldName(String deobFieldName, HashSet<String> possibleBindings) {
            return !isObfuscated() ? deobFieldName : find(getFieldBindings(deobFieldName), possibleBindings);
        }

        public String getParamDeob(String binding) {
            return paramsMapped.get(binding);
        }

        public HashSet<String> getParamBindings(String paramName) {
            return paramsLookup.get(paramName);
        }

        public String getParamName(String deobParamName, HashSet<String> possibleBindings) {
            return !isObfuscated() ? deobParamName : find(getParamBindings(deobParamName), possibleBindings);
        }

        public void registerMethod(String obfBinding, String deobfBinding) {
            registerBinding(methodsMapped, methodsLookup, obfBinding, deobfBinding);
        }

        public void registerField(String obfBinding, String deobfBinding) {
            registerBinding(fieldsMapped, fieldsLookup, obfBinding, deobfBinding);
        }

        public void registerParam(String obfBinding, String deobfBinding) {
            registerBinding(paramsMapped, paramsLookup, obfBinding, deobfBinding);
        }

    }

}
